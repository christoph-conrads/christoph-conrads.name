#!/bin/bash

# Copyright 2016, 2019 Christoph Conrads
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -o pipefail
set -u


# define functions
function mywget() {
	wget \
		--no-clobber \
		--domains="${host}" \
		--no-verbose --append-output=wget.log \
		"$@"
}



function strip_protocol() {
	sed --regexp-extended --expression='s/^https?:\/\///' <<<"$1"
}



function escape_url() {
	sed -e 's/\//\\\//g' <<<"$1"
}



# read input
if [[ $# -lt 2 || $# -gt 3 ]]
then
	echo 'usage: make-static-website <source URL> <target URL> [user@host]'
	exit 1
fi


source_url="$1"
target_url="$2"
if [[ $# -eq 3 ]]
then
	ssh_host="$3"
fi


host="$(strip_protocol "${source_url}")"
target="$(strip_protocol "${target_url}")"

escaped_source_url="$(escape_url "${source_url}")"
escaped_target_url="$(escape_url "${target_url}")"



# prepare working directory
tmp_name_pattern="make-static-website.$(date '+%Y%m%dT%H%M%S').XXXXXXXXXX"
tmpdir="$(mktemp --tmpdir --directory -- "${tmp_name_pattern}")"

echo "${tmpdir}"

cd -- "${tmpdir}"


touch wget.log



# download website
mywget --force-directories -- "${source_url}/sitemap.xml"
grep \
	--extended-regexp --only-matching "${source_url}[^<]+[.]xml" \
	-- "${host}/sitemap.xml" | \
	mywget --input-file=- --force-directories


set +e
	mywget \
		--no-cookie \
		--recursive --execute robots=off \
		--content-on-error \
		--no-parent \
		--page-requisites \
		-- "${source_url}"

	mywget --force-directories --content-on-error -- "${source_url}/404/"
set -e

mywget --level=1 --recursive -- "${source_url}/sitemap.html"
mywget --force-directories -- "${source_url}/robots.txt"
mywget --force-directories -- "${source_url}/wp-content/plugins/google-sitemap-generator/sitemap.xsl"



# remove query parameters from filenames
rm -- "${host}/wp-login.php?action=lostpassword"

for ext in $(tr " " "\n" <<<'css js svg')
do
	files_with_ver="$(find "${host}/" -type f -name "*.${ext}\?ver=*")"

	for file in $files_with_ver
	do
		mv -- "${file}" "${file%%\?*}"
	done
done



# fix links
find "${host}/" \
	-type f \
	-exec sed -i -e "s/${escaped_source_url}/${escaped_target_url}/g" {} \;



# update online website
# do not use --delete unless you supply a .htaccess
if [[ $# -eq 3 ]]
then
	rsync \
		--archive \
		--compress --rsh=ssh \
		-- "${host}/" "${ssh_host}":"${target}-latest"
fi
