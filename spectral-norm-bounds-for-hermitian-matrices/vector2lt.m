% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
function L = vector2lt(x)
	narginchk(1, 1);
	nargoutchk(0, 1);

	m = length(x);
	n = round( sqrt(2*m + 0.25) - 0.5 );

	indices = tril( true(n) );

	L = zeros(n);
	L(indices) = x;
end
