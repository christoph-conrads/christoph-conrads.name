% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
function [ns, rs, xs, nfs] = maximize_norm_ratio_sym(M, n_iter)
	narginchk(2, 2);

	rng(1);

	ns = 2.^(1:M);
	rs = -inf(1, M);
	xs = cell(1, M);
	nfs = zeros(1, M);

	for i=1:length(ns)
		n = ns(i);
		m = n * (n+1) / 2;

		for j=1:n_iter
			x0 = randi( [-n, +n], m, 1 );
			stopit = [1e-3, 1e5, inf, nan, 0];
			[x, fmax, nf] = mdsmax( @eval_norms, x0, stopit );

			if fmax > rs(i)
				fprintf( 1, 'n=%2d fmax=%.17f\n', n, fmax / sqrt(n) );
				xs{i} = x;
			end

			rs(i) = max( fmax, rs(i) );
			nfs(i) = nfs(i) + nf;

			if rs(i) == sqrt(n)
				break
			end
		end

		nfs(i) = nfs(i) / j;
	end

end



function r = eval_norms(x)
	narginchk(1, 1);

	L = vector2lt(x);
	A = L + tril(L, -1)';
	r = norm(A, 1) / norm(A, 2);
end
