#!/bin/bash


set -e



fill_in_data=$(cat superLU_vs_DS.2a2d.stdout.dat)
data=$(cat superLU_vs_DS.c157.stdout.dat)


for solver in "LU" "LL" "DS"
do
	filename_fill_in="fill-in.${solver}.dat"
	filename_max_mem="max-memory.${solver}.dat"
	filename_setup_time="setup-time.${solver}.dat"


	echo "n fill-in" >"${filename_fill_in}"

	fgrep "${solver}" <<<"${fill_in_data}" | \
		awk '{print $2, $5}' |
		sort -g --key=1 --unique >>"${filename_fill_in}"


	echo "n max-memory" >"${filename_max_mem}"

	fgrep "${solver}" <<<"${data}" | \
		awk '{print $2, $5}' |
		sort -g --key=1 --unique >>"${filename_max_mem}"


	echo "n wc-time cpu-time" >"${filename_setup_time}"

	fgrep "${solver}" <<<"${data}" | \
		awk '{if(($6 != "nan") && ($6 != "0.0")) print $2, $6, $7}' |
		sort -g --key=1 --unique >>"${filename_setup_time}"


	num_fails=$(fgrep "${solver}" <<<"${data}" | fgrep nan | wc -l)

	echo "${solver}" ${num_fails} unsolved
done

python2 make-solve-plot-data.py superLU_vs_DS.c157.stdout.dat


make
