#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import csv

import numpy as NP
import numpy.random

import copy

import sys



class Record:
    def __init__(self, \
            filename, n, solver, \
            mem_MB, fill_in, \
            wc_time_setup, cpu_time_setup, \
            n_rhs, wc_time_solve, cpu_time_solve):
        self.filename = filename
        self.n = n
        self.solver = solver
        self.mem_MB = mem_MB
        self.fill_in = fill_in
        self.wc_time_setup = wc_time_setup
        self.cpu_time_setup = cpu_time_setup
        self.n_rhs = n_rhs
        self.wc_time_solve = wc_time_solve
        self.cpu_time_solve = cpu_time_solve



def read_input(filename):
    def row2record(i, row):
        try:
            return Record( \
                row[0], int(row[1]), row[2], \
                int(row[3]), float(row[4]), \
                float(row[5]), float(row[6]),
                int(row[7]), float(row[8]), float(row[9]))
        except Exception as e:
            print '{0}:{1} invalid: {2}'.format(filename, i, str(e))
        else:
            print '{0}:{1} invalid'.format(filename, i)


    with open(filename, 'rb') as f:
        reader = csv.reader(f, delimiter=' ', skipinitialspace=True)
        return map(lambda x: row2record(x[0], x[1]), enumerate(reader))



def main(argv=[]):
    if len(argv) != 2:
        print 'usage: python {0} <csv file>'.format(argv[0])
        return 1

    filename = argv[1]
    records = read_input(filename)

    unique = lambda xs: list(set(xs))
    filenames = unique(map(lambda r: r.filename, records))
    solvers = unique(map(lambda r: r.solver, records))

    assert len(solvers) == 3
    assert 'LU' in solvers
    assert 'LL' in solvers
    assert 'DS' in solvers


    # filter zero solve times, unsolved problems
    def filter_zero_solve_times(rs):
        def filter_function(r, t_min=1.0):
            if r.wc_time_solve < t_min or r.cpu_time_solve <= t_min:
                return True

            if r.solver != 'LL' or r.n_rhs != 256:
                return False

            return NP.isnan(r.wc_time_solve)

        zs = filter(filter_function, rs)
        fs = set( map(lambda r: r.filename, zs) )

        ok = filter(lambda r: r.filename not in fs, rs)

        return ok


    def filter_nans(rs):
        return filter(lambda r: not NP.isnan(r.wc_time_solve), rs)


    records = filter_zero_solve_times(records)
    records = filter_nans(records)


    def get_LL_256_solve_times(rs):
        xs = filter(lambda r: r.solver == 'LL' and r.n_rhs == 256, rs)

        ts = dict()
        for x in xs:
            assert x.filename not in ts.keys()
            ts[x.filename] = x

        return ts

    ll_256 = get_LL_256_solve_times(records)


    # normalize solve times
    def normalize_solve_time(r):
        filename = r.filename
        l = ll_256[filename]

        s = copy.copy(r)
        s.wc_time_solve = s.wc_time_solve / l.wc_time_solve
        s.cpu_time_solve = s.cpu_time_solve / l.cpu_time_solve

        return s

    ns = map(normalize_solve_time, records)


    # output data
    def make_cartesian_coordinates(r):
        x = NP.log2(r.n_rhs)
        y = r.wc_time_solve
        z = r.cpu_time_solve

        return [x, y, z]


    def show_coordinates(xyz):
        fmt = '{:e} {:e} {:e}'

        return fmt.format( *xyz )



    for solver in solvers:
        ms = filter(lambda r: r.solver == solver, ns)

        xyz = NP.array( map(make_cartesian_coordinates, ms) )
        assert xyz.shape[1] == 3
        assert xyz.shape[0] == len(ms)


        # perturb x coordinate
        random = numpy.random.RandomState(seed=1)
        n = xyz.shape[0]
        xyz[:,0] = xyz[:,0] + 0.25 * random.uniform( -1, +1, n )

        # sort by n-rhs
        i = NP.argsort(xyz[:,0])
        xyz = xyz[i,:]
        del i

        strings = map(show_coordinates, xyz)

        filename = 'relative-solve-times.{:s}.dat'.format(solver)
        with open(filename, 'w') as f:
            header = '{:>12s} {:>12s} {:>12s}\n'

            f.write( header.format('n-rhs', 'rel-wc-time', 'rel-cpu-time') )
            f.write( '\n'.join(strings) )

        del f
        del filename

    return 0



if __name__ == '__main__':
    sys.exit( main(sys.argv) )
