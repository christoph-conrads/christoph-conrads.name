files = {
	'bcsstk01';
	'bcsstk02';
	'bcsstk03';
	'bcsstk04';
	'bcsstk05';
	'bcsstk06';
	'bcsstk08';
	'bcsstk09';
	'bcsstk10';
	'bcsstk11';
	'bcsstk13';
	'bcsstk14';
	'bcsstk15';
	'bcsstk16';
	'bcsstk19';
	'bcsstk20';
	'bcsstk21';
	'bcsstk22';
	'bcsstk23';
	'bcsstk24';
	'bcsstk26';
	'bcsstk27';
	'bcsstk28';
};



make_path = @(s) sprintf('matrices/%s.mtx', s);


for i=1:length(files)
	k_path = make_path( files{i} );

	K = mmread( k_path );
	K = full(K);
	T = hess(K);

	x = spdiags( T, [0 -1] );
	d = x(:,1);
	e = x(:,2);
	data = [1:length(K); x'];
	clear T x d e

	outfile = sprintf( '%s.dat', files{i} );

	fid = fopen( outfile, 'w+t' );
	if fid < 0
		error( ferror(fid) );
	end

	fprintf( fid, '%d\n', length(K) );
	fprintf( fid, '%4d %.17e %+.17e\n', data );

	if fclose(fid) < 0
		error( ferror(fid) );
	end
end
