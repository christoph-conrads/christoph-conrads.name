# Christoph Conrads 2014-12-27

function remove_last_char(str, char)
{
	l = length(str);

	if( index(str, char) != l )
		printf( \
			"character mismatch ['%s', '%s'] (line %d)\n", \
			str, char, FNR) > "/dev/stderr"

	return substr( str, 1, length(str)-1 )
}



function remove_comma(str)
{
	return remove_last_char(str, ",")
}



BEGIN {
	algos[0] = "DSTEQR(COMPZ=I)"
	algos[1] = "DSTEVX(RANGE=A)"
	algos[2] = "DSTEDC(COMPZ=I)"
	algos[3] = "DSTEGR(RANGE=A)"
	algos[4] = "DPTEQR(COMPZ=I)"

	for(i in algos)
	{
		filename = sprintf( "%s.txt", algos[i] )

		printf( \
			"%4s form type cond %10s %10s %10s\n", \
			"n", "time", "residual", "ortho") > filename
	}
}



/Case: / {
	caseID = remove_comma($2)
	n = $5
	form = remove_comma($7)
	type = remove_comma($9)
	cond = remove_comma($11)
	dist = remove_comma($13)
	sign = remove_last_char($15, ")")

	if(type == 5 || type == 6)
		printf( \
			"Expected deterministic type, found type=%d (line %d)\n", \
			type, FNR) > "/dev/stderr"

	if(sign != 0)
		printf( \
			"Expected sign=0, found sign=%d (line %d)\n", \
			sign, FNR) > "/dev/stderr"
}



/INFO > 0 [*]$/ {
	algo = remove_last_char($1, ":")

	filename = sprintf( "%s.txt", algo )

	printf( \
		"%4d %4d %4d %4d %10s %10s %10s\n", \
		n, form, type, cond, "NaN", "NaN", "NaN") > filename
}



/^ .*TIME = [[:digit:]][.]/ {
	algo = remove_last_char($1, ":")
	time = remove_comma($4)
	residual = remove_comma($7)
	ortho = $10

	filename = sprintf( "%s.txt", algo )

	printf( \
		"%4d %4d %4d %4d %s %s %s\n", \
		n, form, type, cond, time, residual, ortho) > filename
}
