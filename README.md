This repository contains data and source code for the blog posts on
https://christoph-conrads.name published before August 2016.

The license is different for every folder.

Folder contents:
* performance-and-accuracy-of-xpteqr:
  Test data for the article [Performance and Accuracy of xPTEQR](https://christoph-conrads.name/performance-and-accuracy-of-xpteqr/)
* spectral-norm-bounds-for-hermitian-matrices:
  Matlab scripts for the article [Spectral Norm Bounds for Hermitian Matrices](https://christoph-conrads.name/spectral-norm-bounds-for-hermitian-matrices/)
* a-fast-static-wordpress-blog:
  scripts for the article [A Fast, Static WordPress Blog](https://christoph-conrads.name/a-fast-static-wordpress-blog/)
* hpsd\_gep\_solvers:
  Source code for the master's thesis "Projection Methods for Generalized Eigenvalue Problems" by Christoph Conrads
